#!/bin/sh

echo "> Downloading & Installing Satellites Frequencies File Please Wait..."
sleep 3

wget --show-progress -qO /etc/tuxbox/satellites.xml https://gitlab.com/eliesat/settings/-/raw/main/flysat/satellites.xml

echo "> satellite.xml file installed successfully"
echo "> your frequencies are up to date"

sleep 3