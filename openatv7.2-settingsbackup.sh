# check boxname#
HOSTNAME=$(head -n 1 /etc/hostname)
rm -rf /hdd/backup_openatv_"$HOSTNAME"

#check media hdd dir#
if [  -d "/hdd/" ]; then
if [ ! -d "/hdd/backup_openatv_"$HOSTNAME" " ]; then
mkdir /hdd/backup_openatv_"$HOSTNAME"
else 
echo''
fi

#check version
VERSION=$(python -c"import platform; print(platform.python_version())")
if [ "$VERSION" == 3.11.0 ]; then echo "> You have "$HOSTNAME" Openatv  python "$VERSION" image "
sleep 3s

#preinst remove#
rm -r /tmp/enigma2settingsbackup
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL
fi
rm -rf /control
rm -rf /postinst
rm -rf /preinst
rm -rf /prerm
rm -rf /postrm
rm -rf /tmp/*.ipk
rm -rf /tmp/*.tar.gz

#download backup#
echo "> Downloading  Openatv 7.2 Settings Backup Please Wait ..."
sleep 3s

wget -O /var/volatile/tmp/openatv7.2-settingsbackup.tar.gz --no-check-certificate "https://gitlab.com/eliesat/settings/-/raw/main/openatv7.2-settingsbackup.tar.gz" 

#extract backup#
tar -xf /var/volatile/tmp/openatv7.2-settingsbackup.tar.gz -C /
MY_RESULT=$?

#remove files from tmp#
rm -f /var/volatile/tmp/openatv7.2-settingsbackup.tar.gz > /dev/null 2>&1
echo ''
if [ $MY_RESULT -eq 0 ]; then 
echo "> Backup Extracted Successfully"
else
echo "> Backup Extracting Failed"
fi

#backup conf#
{
    echo "config.plugins.MyMetrixLiteOther.EHDtested="$HOSTNAME"_|_01"
    
} >>/tmp/enigma2settingsbackup/etc/enigma2/settings

cd /tmp/enigma2settingsbackup

set -e
tar  -zcvf enigma2settingsbackup.tar.gz *

cp -r /tmp/enigma2settingsbackup/enigma2settingsbackup.tar.gz /media/hdd/backup_openatv_"$HOSTNAME"
rm -rf /tmp/enigma2settingsbackup

set +e

if [  -f "/hdd/backup_openatv_"$HOSTNAME"/enigma2settingsbackup.tar.gz" ]; then 
echo "> Your openatv settings are ready to use
> goto menu/setup/softwaremanagemant/restoresystemsettings and press ok to restore"
else 
echo'> Your settings backup failed '
fi



fi

else
echo "Mount your external memory >media/hdd<  and try again "
sleep 5s
fi
exit 0