#!/bin/sh
echo "> Downloading motorised tuner config file  Please Wait ..."
sleep 3s

wget -O /tmp/tuner "https://gitlab.com/eliesat/settings/-/raw/main/tuner/tuner"

echo "> installing motorised tuner settings  Please Wait ..."
sleep 3
echo "> your device will restart now please wait..."
init 4
sleep 1s
sed -i '/config.Nims.0/d' /etc/enigma2/settings
grep "config.Nims.*" /tmp/tuner >> /etc/enigma2/settings
rm -rf /tmp/tuner > /dev/null 2>&1
sleep 1s
init 3

exit 0