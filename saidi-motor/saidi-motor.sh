#!/bin/sh

#configuration
#######################################
channel=saidi-motor
url=https://gitlab.com/eliesat/settings/-/raw/main/saidi-motor

# Remove unnecessary files and folders
#######################################
[ -d "/CONTROL" ] && rm -r /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1

#downloading channels lists file
#######################################
echo "> Downloading "$channel" Channels Lists  Please Wait ..."
sleep 3
wget --show-progress $url/$channel.tar.gz -qP /tmp

#removing old channels lists file
#######################################
rm -rf /etc/enigma2/lamedb /etc/enigma2/*list /etc/enigma2/*.tv /etc/enigma2/*.radio  /etc/tuxbox/*.xml >/dev/null 2>&1

#extracting channels lists file
#######################################
cd /tmp
set -e
sleep 3
echo
tar -xzf $channel.tar.gz -C /
set +e
rm -f $channel.tar.gz

# Restart Enigma2 service or kill enigma2 based on the system
#######################################
if [ -f /etc/opkg/opkg.conf ];then
echo
echo "> "$channel" Channels Lists are installed successfully"
wget -qO - http://127.0.0.1/web/servicelistreload?mode=0 > /dev/null 2>&1
sleep 2
echo
init 3
else
echo
sleep 2
echo "> your device will restart now, please wait..."
systemctl restart enigma2
fi